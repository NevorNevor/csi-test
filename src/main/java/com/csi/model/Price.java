package com.csi.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity(name = "prices")
public class Price {

    private static final String SEQ_NAME = "prices_seq";

    @Id
    @SequenceGenerator(name = SEQ_NAME, sequenceName = SEQ_NAME, allocationSize = 1)
    @GeneratedValue(generator = SEQ_NAME, strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "product_code")
    private String productCode;
    private Integer number;
    private Integer depart;
    private Date begin;
    private Date end;
    private Long value;

    public Price() {
    }

    public Price(
            final String productCode,
            final Integer number,
            final Integer depart,
            final Date begin,
            final Date end,
            final Long value) {
        this.productCode = productCode;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public Price(
            final Long id,
            final String productCode,
            final Integer number,
            final Integer depart,
            final Date begin,
            final Date end,
            final Long value) {
        this.id = id;
        this.productCode = productCode;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public Integer getDepart() {
        return depart;
    }

    public void setDepart(final Integer depart) {
        this.depart = depart;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(final Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(final Date end) {
        this.end = end;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(final Long value) {
        this.value = value;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Price anotherPrice = (Price) obj;

        return Objects.equals(id, anotherPrice.getId()) &&
                Objects.equals(productCode, anotherPrice.productCode) &&
                Objects.equals(number, anotherPrice.number) &&
                Objects.equals(depart, anotherPrice.depart) &&
                Objects.equals(begin, anotherPrice.begin) &&
                Objects.equals(end, anotherPrice.end) &&
                Objects.equals(value, anotherPrice.value);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(id);
        result = 31 * result + productCode.hashCode();
        result = 31 * result + number.hashCode();
        result = 31 * result + depart.hashCode();
        result = 31 * result + begin.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", number=" + number +
                ", depart=" + depart +
                ", begin=" + begin +
                ", end=" + end +
                ", value=" + value +
                '}';
    }

    @Override
    public Price clone() {
        return new Price(id, productCode, number, depart, begin, end, value);
    }
}
