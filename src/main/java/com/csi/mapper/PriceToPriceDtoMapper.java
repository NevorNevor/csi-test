package com.csi.mapper;

import com.csi.domain.PriceDto;
import com.csi.model.Price;
import org.springframework.stereotype.Component;

@Component
public class PriceToPriceDtoMapper implements IMapper<PriceDto, Price> {

    @Override
    public PriceDto map(final Price price) {
        final PriceDto result;
        if (price == null) {
            result = null;
        } else {
            result = new PriceDto();
            result.setId(price.getId());
            result.setProductCode(price.getProductCode());
            result.setNumber(price.getNumber());
            result.setDepart(price.getDepart());
            result.setBegin(price.getBegin());
            result.setEnd(price.getEnd());
            result.setValue(price.getValue());
        }
        return result;
    }

}
