package com.csi.mapper;

import com.csi.domain.PriceDto;
import com.csi.model.Price;
import org.springframework.stereotype.Component;

@Component
public class PriceDtoToPriceMapper implements IMapper<Price, PriceDto> {

    @Override
    public Price map(final PriceDto priceDto) {
        final Price result;
        if (priceDto == null) {
            result = null;
        } else {
            result = new Price();
            result.setProductCode(priceDto.getProductCode());
            result.setNumber(priceDto.getNumber());
            result.setDepart(priceDto.getDepart());
            result.setBegin(priceDto.getBegin());
            result.setEnd(priceDto.getEnd());
            result.setValue(priceDto.getValue());
        }
        return result;
    }

}
