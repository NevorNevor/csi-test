package com.csi.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface IMapper<D, E> {

    D map(E subject);

    default List<D> map(final Collection<E> subjects) {
        return subjects.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

}
