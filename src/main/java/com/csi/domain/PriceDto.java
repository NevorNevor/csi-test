package com.csi.domain;

import com.csi.deserializer.PriceJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class PriceDto {

    private Long id;
    @NotNull
    private String productCode;
    @NotNull
    private Integer number;
    @NotNull
    private Integer depart;
    @NotNull
    private Date begin;
    @NotNull
    private Date end;
    @NotNull
    @Min(value = 1)
    private Long value;

    public PriceDto() {
    }

    public PriceDto(final String productCode,
                    final Integer number,
                    final Integer depart,
                    final Date begin,
                    final Date end,
                    final Long value) {
        this.id = id;
        this.productCode = productCode;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public Integer getDepart() {
        return depart;
    }

    public void setDepart(final Integer depart) {
        this.depart = depart;
    }

    public Date getBegin() {
        return begin;
    }

    @JsonDeserialize(using = PriceJsonDateDeserializer.class)
    public void setBegin(final Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    @JsonDeserialize(using = PriceJsonDateDeserializer.class)
    public void setEnd(final Date end) {
        this.end = end;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(final Long value) {
        this.value = value;
    }

}
