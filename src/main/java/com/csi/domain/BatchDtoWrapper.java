package com.csi.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

public class BatchDtoWrapper<E> {

    @Valid
    @Size(min = 1)
    private List<E> batch;

    @JsonValue
    public List<E> getBatch() {
        return batch;
    }

    @JsonCreator
    public void setBatch(final List<E> batch) {
        this.batch = batch;
    }
}
