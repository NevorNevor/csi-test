package com.csi.service;

import com.csi.domain.PriceDto;

import java.util.List;

public interface IPriceService {

    List<PriceDto> findAll();

    void createPriceBatch(List<PriceDto> priceList);

}
