package com.csi.service.impl;

import com.csi.domain.PriceDto;
import com.csi.group.PriceTypeGroup;
import com.csi.mapper.PriceDtoToPriceMapper;
import com.csi.mapper.PriceToPriceDtoMapper;
import com.csi.model.Price;
import com.csi.repository.IPriceRepository;
import com.csi.service.IPriceService;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static com.csi.function.FunctionUtils.not;
import static com.csi.function.FunctionUtils.notNull;
import static java.util.stream.Collectors.*;

@Service
public class PriceService implements IPriceService {

    private final IPriceRepository priceRepository;
    private final PriceToPriceDtoMapper priceToPriceDtoMapper;
    private final PriceDtoToPriceMapper priceDtoToPriceMapper;

    @Autowired
    public PriceService(final IPriceRepository priceRepository,
                        final PriceToPriceDtoMapper priceToPriceDtoMapper,
                        final PriceDtoToPriceMapper priceDtoToPriceMapper) {
        this.priceRepository = priceRepository;
        this.priceToPriceDtoMapper = priceToPriceDtoMapper;
        this.priceDtoToPriceMapper = priceDtoToPriceMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PriceDto> findAll() {
        return priceToPriceDtoMapper.map(
                priceRepository.findAll()
        );
    }

    @Override
    @Transactional
    public void createPriceBatch(final List<PriceDto> priceList) {
        final List<Price> newPriceList = priceDtoToPriceMapper.map(priceList);
        final List<Price> oldPriceList = priceRepository.findAllAssignedWithCollection(newPriceList);
        final Collection<Price> newPrices;
        if (!oldPriceList.isEmpty()) {
            final Set<Price> merged = mergePriceLists(oldPriceList, newPriceList);
            final Set<Price> priceForDeleting = merged.stream()
                    .filter(price -> price.getBegin().equals(price.getEnd()) && price.getId() != null)
                    .collect(toSet());
            newPrices = merged.stream()
                    .filter(not(priceForDeleting::contains))
                    .filter(price -> !price.getBegin().equals(price.getEnd()))
                    .collect(toSet());
            final Set<Long> priceIdsForDelete = priceForDeleting.stream()
                    .map(Price::getId)
                    .collect(toSet());
            priceRepository.deleteAllByIdIn(priceIdsForDelete);
        } else {
            newPrices = newPriceList;
        }
        priceRepository.save(newPrices);
    }

    /**
     * Main merge prices method
     *
     * @param oldPriceList prices from db
     * @param newPriceList prices for import
     * @return merged prices
     */
    private Set<Price> mergePriceLists(final Collection<Price> oldPriceList, final Collection<Price> newPriceList) {
        final Map<PriceTypeGroup, List<Price>> newGroupedByPriceTypePair = newPriceList.stream()
                .distinct()
                .collect(groupingBy(PriceTypeGroup::new));
        final Map<PriceTypeGroup, List<Price>> oldGroupedByPriceTypePair = oldPriceList.stream()
                .collect(groupingBy(PriceTypeGroup::new));
        final Map<PriceTypeGroup, Set<Price>> merged = oldGroupedByPriceTypePair.entrySet().stream()
                .map(priceTypeGroupEntry -> {
                    final PriceTypeGroup priceTypeGroup = priceTypeGroupEntry.getKey();
                    final List<Price> oldPrices = priceTypeGroupEntry.getValue();
                    return oldPrices.stream().map(price -> {
                        final List<Price> assignablePrices = newGroupedByPriceTypePair.get(priceTypeGroup);
                        final Set<Price> connectedPrices = assignablePrices.stream()
                                .filter(assignablePrice -> isPricesConnected(price, assignablePrice))
                                .collect(toSet());
                        return new Pair<>(priceTypeGroup, mergePrices(price, connectedPrices));
                    }).collect(toSet());
                }).flatMap(Set::stream)
                .collect(getPairMapCollector());
        final Map<PriceTypeGroup, Set<Price>> mergedPriceTypeGroups = merged.entrySet().stream()
                .map(mergedPriceTypeGroup -> {
                    final PriceTypeGroup priceTypeGroup = mergedPriceTypeGroup.getKey();
                    final Set<Price> mergedPrices = mergedPriceTypeGroup.getValue();
                    final Set<Price> mergedWithoutOldDuplicates = mergedPrices
                            .stream()
                            .map(price -> processOldDuplicates(price, mergedPrices))
                            .collect(toSet());
                    final Set<Price> filteredMerged = mergedWithoutOldDuplicates.stream()
                            .map(price -> excludeInnerCross(price, mergedWithoutOldDuplicates))
                            .filter(price -> !isProlongedPrice(price, mergedPrices))
                            .collect(toSet());
                    return new Pair<>(priceTypeGroup, removeRedundantIds(filteredMerged));
                })
                .collect(getPairMapCollector());
        return newGroupedByPriceTypePair.entrySet().stream()
                .map(priceTypeGroupEntry -> {
                    final Set<Price> result = new HashSet<>();
                    final PriceTypeGroup priceTypeGroup = priceTypeGroupEntry.getKey();
                    final List<Price> prices = priceTypeGroupEntry.getValue();
                    final Set<Price> mergedPrices = mergedPriceTypeGroups.get(priceTypeGroup);
                    if (Objects.nonNull(mergedPrices)) {
                        final Set<Price> notConnectedNewPrices = prices.stream()
                                .filter(price1 -> mergedPrices.stream()
                                        .noneMatch(price2 -> isPricesConnected(price1, price2))
                                ).collect(toSet());
                        result.addAll(notConnectedNewPrices);
                        result.addAll(mergedPrices);
                    }
                    return result;
                }).flatMap(Set::stream)
                .collect(toSet());
    }

    /**
     * Collect pairs to map with flattern pair values
     *
     * @return
     */
    private Collector<Pair<PriceTypeGroup, Set<Price>>, ?, Map<PriceTypeGroup, Set<Price>>> getPairMapCollector() {
        return toMap(
                Pair::getKey,
                Pair::getValue,
                (prices1, prices2) -> Stream.of(prices1, prices2)
                        .flatMap(Set::stream)
                        .collect(toSet())
        );
    }

    /**
     * Remove redundant id from old db prices
     *
     * @param priceList with duplicates ids
     * @return processed prices set
     */
    private Set<Price> removeRedundantIds(final Collection<Price> priceList) {
        final Map<Optional<Long>, List<Price>> groupedById = priceList.stream()
                .collect(groupingBy(price -> Optional.ofNullable(price.getId())));
        return groupedById.entrySet().stream()
                .map(priceIdGroup -> {
                    List<Price> result = priceIdGroup.getValue();
                    if (priceIdGroup.getKey().isPresent()) {
                        final List<Price> sameIdsPriceList = priceIdGroup.getValue();
                        final int sameIdsPriceListSize = sameIdsPriceList.size();
                        if (sameIdsPriceListSize > 1) {
                            final Price lastPrice = sameIdsPriceList.get(sameIdsPriceListSize - 1);
                            result = Stream.concat(
                                    sameIdsPriceList.stream()
                                            .limit(sameIdsPriceListSize - 1)
                                            .map(price -> {
                                                final Price newPrice = price.clone();
                                                newPrice.setId(null);
                                                return newPrice;
                                            }),
                                    Stream.of(lastPrice)
                            ).collect(toList());
                        }
                    }
                    return result;
                }).flatMap(List::stream)
                .collect(toSet());
    }

    /**
     * @param price
     * @param priceList
     * @return true if @price is prolonged for one of @priceList
     */
    private boolean isProlongedPrice(final Price price, final Set<Price> priceList) {
        boolean result = false;
        if (Objects.isNull(price.getId())) {
            result = priceList.stream()
                    .filter(notNull(Price::getId))
                    .filter(oldPrice -> oldPrice.getValue().equals(price.getValue()))
                    .filter(oldPrice ->
                            price.getBegin().equals(oldPrice.getBegin()) ||
                                    price.getEnd().equals(oldPrice.getEnd())
                    )
                    .anyMatch(
                            oldPrice ->
                                    price.getBegin().after(oldPrice.getBegin()) ||
                                            price.getEnd().before(oldPrice.getEnd())
                    );
        }
        return result;
    }

    /**
     * Cut old prices switched by new
     *
     * @param price
     * @param priceList
     * @return processed price
     */
    private Price processOldDuplicates(final Price price, final Collection<Price> priceList) {
        Price result = price;
        if (Objects.nonNull(price.getId())) {
            final Set<Price> sameIdsPriceList = priceList.stream()
                    .filter(priceFromList -> Objects.equals(priceFromList.getId(), price.getId()))
                    .collect(toSet());
            final Optional<Price> firstConnectedOptional = sameIdsPriceList.stream()
                    .filter(priceFromList -> !Objects.equals(priceFromList, price))
                    .filter(priceFromList -> isPricesConnected(priceFromList, price))
                    .findFirst();
            if (firstConnectedOptional.isPresent()) {
                final Price connectedPrice = firstConnectedOptional.get();
                final Date begin = price.getBegin().after(connectedPrice.getBegin()) ?
                        price.getBegin() : connectedPrice.getBegin();
                final Date end = price.getEnd().before(connectedPrice.getEnd()) ?
                        price.getEnd() : connectedPrice.getEnd();
                result = price.clone();
                result.setBegin(begin);
                result.setEnd(end);
            }
        }
        return result;
    }

    /**
     * @param price1
     * @param price2
     * @return true if @price1 one connected with @price2
     */
    private boolean isPricesConnected(final Price price1, final Price price2) {
        final Date begin1 = price2.getBegin();
        final Date end1 = price2.getEnd();
        final Date begin2 = price1.getBegin();
        final Date end2 = price1.getEnd();
        return begin1.after(begin2) && begin1.before(end2) ||
                end1.after(begin2) && end1.before(end2) ||
                begin2.after(begin1) && begin2.before(end1) ||
                end2.after(begin1) && end2.before(end1);
    }

    /**
     * @param price
     * @param priceList
     * @return marked as deletable price if redundant
     */
    private Price excludeInnerCross(final Price price, final Collection<Price> priceList) {
        Price result = price;
        boolean isCrossed = priceList.stream()
                .filter(priceFromList -> !Objects.equals(priceFromList, price))
                .filter(priceFromList -> Objects.isNull(priceFromList.getId()))
                .anyMatch(priceFromList ->
                        price.getBegin().equals(priceFromList.getBegin()) &&
                                price.getEnd().before(priceFromList.getEnd()) ||
                                price.getEnd().equals(priceFromList.getEnd()) &&
                                        price.getBegin().after(priceFromList.getBegin())
                );
        if (isCrossed) {
            result = price.clone();
            markPriceAsDeletable(result);
        } else if (!Objects.isNull(price.getId())) {
            isCrossed = priceList.stream()
                    .filter(priceFromList -> !Objects.equals(priceFromList, price))
                    .filter(priceFromList -> Objects.equals(priceFromList.getId(), price.getId()))
                    .filter(priceFromList ->
                            priceFromList.getBegin().equals(price.getBegin()) ||
                                    priceFromList.getEnd().equals(price.getEnd()))
                    .anyMatch(priceFromList ->
                            priceFromList.getBegin().after(price.getBegin()) ||
                                    priceFromList.getEnd().before(price.getEnd()));
            if (isCrossed) {
                result = price.clone();
                markPriceAsDeletable(result);
            }
        }
        return result;
    }

    /**
     * Mark price for delete (begin = end)
     *
     * @param price
     */
    private void markPriceAsDeletable(@NotNull final Price price) {
        price.setBegin(price.getEnd());
    }

    /**
     * Process new prices with old
     *
     * @param oldPrice
     * @param newPriceList
     * @return set of processed prices
     */
    private Set<Price> mergePrices(final Price oldPrice, final Collection<Price> newPriceList) {
        final Set<Price> result = new HashSet<>();
        for (Price newPrice : newPriceList) {
            if (newPrice.getBegin().before(oldPrice.getBegin())) {
                //|------new---------|
                //           |-----old-----|
                if (newPrice.getEnd().before(oldPrice.getEnd())) {
                    if (oldPrice.getValue().equals(newPrice.getValue())) {
                        final Price price = oldPrice.clone();
                        price.setBegin(newPrice.getBegin());
                        result.add(price);
                    } else {
                        result.add(newPrice.clone());
                        final Price price = oldPrice.clone();
                        price.setBegin(plusSecond(newPrice.getEnd()));
                        result.add(price);
                    }
                    //|---------------new------------|
                    //           |-----old-----|
                } else {
                    result.add(newPrice.clone());
                    final Price price = oldPrice.clone();
                    markPriceAsDeletable(price);
                    result.add(price);
                }
            } else {
                //      |----new----|
                //  |-----old-------------|
                if (newPrice.getEnd().before(oldPrice.getEnd())) {
                    if (oldPrice.getValue().equals(newPrice.getValue())) {
                        result.add(oldPrice.clone());
                    } else {
                        final Price price1 = oldPrice.clone();
                        price1.setEnd(minusSecond(newPrice.getBegin()));
                        result.add(price1);
                        result.add(newPrice.clone());
                        final Price price2 = oldPrice.clone();
                        price2.setBegin(plusSecond(newPrice.getEnd()));
                        result.add(price2);
                    }
                } else {
                    //      |--------new--------|
                    //  |-----old------|
                    if (oldPrice.getValue().equals(newPrice.getValue())) {
                        final Price price = oldPrice.clone();
                        price.setEnd(newPrice.getEnd());
                        result.add(price);
                    } else {
                        final Price price = oldPrice.clone();
                        price.setEnd(minusSecond(newPrice.getBegin()));
                        result.add(price);
                        result.add(newPrice.clone());
                    }
                }
            }
        }
        return result;
    }

    private Date plusSecond(@NotNull final Date date) {
        return new Date(date.getTime() + 1000);
    }

    private Date minusSecond(@NotNull final Date date) {
        return new Date(date.getTime() - 1000);
    }

}
