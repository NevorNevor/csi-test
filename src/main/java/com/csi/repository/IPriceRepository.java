package com.csi.repository;

import com.csi.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Predicate;
import java.util.Collection;
import java.util.List;

import static org.springframework.data.jpa.domain.Specifications.where;

public interface IPriceRepository extends JpaRepository<Price, Long>, JpaSpecificationExecutor<Price> {

    void deleteAllByIdIn(Collection<Long> ids);

    default List<Price> findAllAssignedWithCollection(final Collection<Price> prices) {
        return findAll(where((root, query, cb) ->
                        cb.or(
                                prices.stream().map(price ->
                                        cb.and(
                                                cb.equal(root.get("productCode"), price.getProductCode()),
                                                cb.equal(root.get("depart"), price.getDepart()),
                                                cb.equal(root.get("number"), price.getNumber()),
                                                cb.or(
                                                        cb.and(
                                                                cb.greaterThanOrEqualTo(
                                                                        root.get("begin"), price.getBegin()),
                                                                cb.lessThanOrEqualTo(root.get("begin"), price.getEnd())
                                                        ),
                                                        cb.and(
                                                                cb.greaterThanOrEqualTo(
                                                                        root.get("end"), price.getBegin()),
                                                                cb.lessThanOrEqualTo(root.get("end"), price.getEnd())
                                                        ),
                                                        cb.and(
                                                                cb.lessThanOrEqualTo(
                                                                        root.get("begin"), price.getBegin()),
                                                                cb.greaterThanOrEqualTo(
                                                                        root.get("end"), price.getBegin())
                                                        ),
                                                        cb.and(
                                                                cb.lessThanOrEqualTo(
                                                                        root.get("begin"), price.getEnd()),
                                                                cb.greaterThanOrEqualTo(root.get("end"), price.getEnd())
                                                        )
                                                )
                                        )
                                ).toArray(Predicate[]::new)
                        )
                )
        );
    }

}
