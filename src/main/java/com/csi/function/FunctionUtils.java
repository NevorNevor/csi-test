package com.csi.function;

import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionUtils {

    private FunctionUtils() {
    }

    public static <T> Predicate<T> not(final Predicate<T> predicate) {
        return predicate.negate();
    }

    public static <T, R> Predicate<T> notNull(final Function<T, R> function) {
        return (T t) -> function.apply(t) != null;
    }

}
