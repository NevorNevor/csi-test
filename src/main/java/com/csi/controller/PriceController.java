package com.csi.controller;

import com.csi.domain.BatchDtoWrapper;
import com.csi.domain.PriceDto;
import com.csi.service.IPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/price")
@RestController
public class PriceController {

    private final IPriceService priceService;

    @Autowired
    public PriceController(final IPriceService priceService) {
        this.priceService = priceService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PriceDto> findAll() {
        return priceService.findAll();
    }

    @RequestMapping(path = "batch", method = RequestMethod.POST)
    public void createBatch(@Valid @RequestBody final BatchDtoWrapper<PriceDto> prices) {
        priceService.createPriceBatch(
                prices.getBatch()
        );
    }

}
