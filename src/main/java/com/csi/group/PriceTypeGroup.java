package com.csi.group;

import com.csi.model.Price;

public class PriceTypeGroup {

    private final String productCode;
    private final Integer number;
    private final Integer depart;

    public PriceTypeGroup(final Price price) {
        productCode = price.getProductCode();
        number = price.getNumber();
        depart = price.getDepart();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final PriceTypeGroup priceTypeGroup = getClass().cast(o);

        return productCode.equals(priceTypeGroup.productCode) &&
                number.equals(priceTypeGroup.number) &&
                depart.equals(priceTypeGroup.depart);
    }

    @Override
    public int hashCode() {
        int result = productCode != null ? productCode.hashCode() : 0;
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (depart != null ? depart.hashCode() : 0);
        return result;
    }
}
