package com.csi.service.impl;

import com.csi.domain.PriceDto;
import com.csi.model.Price;
import com.csi.repository.IPriceRepository;
import com.csi.service.IPriceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public final class PriceServiceTest {

    private static final SimpleDateFormat priceFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    @MockBean
    private IPriceRepository priceRepositoryMock;

    @Autowired
    private IPriceService priceService;

    /**
     * |------old(v=1)------|------old(v=2)------|
     * _________|---------new(v=3)-----|_________
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch1Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("31.01.2017 23:59:59"), 1L),
                new Price(2L, "1", 1, 1, priceFormat.parse("01.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("19.01.2017 23:59:59"), 1L),
                new Price("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 3L),
                new Price(2L, "1", 1, 1, priceFormat.parse("21.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * |------old(v=1)------|------old(v=2)------|
     * ________|---------new(v=1)-----|___________
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch2Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("31.01.2017 23:59:59"), 1L),
                new Price(2L, "1", 1, 1, priceFormat.parse("01.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 1L),
                new Price(2L, "1", 1, 1, priceFormat.parse("21.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * |------old(v=2)------|------old(v=1)------|
     * ________|---------new(v=1)-----|___________
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch3Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("31.01.2017 23:59:59"), 2L),
                new Price(2L, "1", 1, 1, priceFormat.parse("01.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("19.01.2017 23:59:59"), 2L),
                new Price(2L, "1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * _________|--------old(v=3)--------|________
     * |-------new(v=1)----|---------new(v=2)-----|
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch4Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("31.01.2017 23:59:59"), 1L),
                new PriceDto("1", 1, 1, priceFormat.parse("01.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("31.01.2017 23:59:59"), 1L),
                new Price("1", 1, 1, priceFormat.parse("01.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(1)).deleteAllByIdIn(eq(Collections.singleton(1L)));
    }

    /**
     * _________|--------old(v=3)--------|________
     * |----new(v=1)---|______|-----new(v=2)-----|
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch5Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("15.01.2017 00:00:00"),
                        priceFormat.parse("25.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("20.01.2017 23:59:59"), 1L),
                new PriceDto("1", 1, 1, priceFormat.parse("20.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("20.01.2017 23:59:59"), 1L),
                new Price(1L, "1", 1, 1, priceFormat.parse("21.01.2017 00:00:00"),
                        priceFormat.parse("19.02.2017 23:59:59"), 3L),
                new Price("1", 1, 1, priceFormat.parse("20.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * |---------------old(v=3)------------------|
     * ___|---new(v=1)---|____|---new(v=2)---|___
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch6Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("05.01.2017 00:00:00"),
                        priceFormat.parse("20.01.2017 23:59:59"), 1L),
                new PriceDto("1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("04.01.2017 23:59:59"), 3L),
                new Price("1", 1, 1, priceFormat.parse("05.01.2017 00:00:00"),
                        priceFormat.parse("20.01.2017 23:59:59"), 1L),
                new Price("1", 1, 1, priceFormat.parse("21.01.2017 00:00:00"),
                        priceFormat.parse("04.02.2017 23:59:59"), 3L),
                new Price("1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 2L),
                new Price(1L, "1", 1, 1, priceFormat.parse("21.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * |---------------old(v=1)------------------|
     * ___|---new(v=1)---|____|---new(v=2)---|___
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch7Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("05.01.2017 00:00:00"),
                        priceFormat.parse("20.01.2017 23:59:59"), 1L),
                new PriceDto("1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("04.02.2017 23:59:59"), 1L),
                new Price("1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("20.02.2017 23:59:59"), 2L),
                new Price(1L, "1", 1, 1, priceFormat.parse("21.02.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 1L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * ___|---old(v=1)---|____|---old(v=2)---|___
     * |---------------new(v=3)------------------|
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch8Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("05.01.2017 00:00:00"),
                        priceFormat.parse("25.01.2017 23:59:59"), 1L),
                new Price(2L, "1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("25.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("01.01.2017 00:00:00"),
                        priceFormat.parse("28.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(1)).deleteAllByIdIn(
                eq(Stream.of(1L, 2L).collect(Collectors.toSet())));
    }

    /**
     * ___|-old(v=1)-|________________|-old(v=2)-|___
     * _________________|-new(v=3)-|_________________
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch9Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "1", 1, 1, priceFormat.parse("05.01.2017 00:00:00"),
                        priceFormat.parse("15.01.2017 23:59:59"), 1L),
                new Price(2L, "1", 1, 1, priceFormat.parse("05.02.2017 00:00:00"),
                        priceFormat.parse("15.02.2017 23:59:59"), 2L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection())).thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("01.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toList());

        priceService.createPriceBatch(newPriceList);

        final Set<Price> expected = Stream.of(
                new Price("1", 1, 1, priceFormat.parse("20.01.2017 00:00:00"),
                        priceFormat.parse("01.02.2017 23:59:59"), 3L)
        ).collect(Collectors.toSet());

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

    /**
     * CSI data
     *
     * @throws Exception
     */
    @Test
    public void createPriceBatch10Test() throws Exception {

        final List<Price> oldPriceList = Stream.of(
                new Price(1L, "122856", 1, 1,
                        priceFormat.parse("01.01.2013 00:00:00"), priceFormat.parse("31.01.2013 23:59:59"), 11000L),
                new Price(2L, "122856", 2, 1, priceFormat.parse("10.01.2013 00:00:00"),
                        priceFormat.parse("20.01.2013 23:59:59"), 99000L),
                new Price(3L, "6654", 1, 2, priceFormat.parse("01.01.2013 00:00:00"),
                        priceFormat.parse("31.01.2013 00:00:00"), 5000L)
        ).collect(Collectors.toList());

        when(priceRepositoryMock.findAllAssignedWithCollection(anyCollection()))
                .thenReturn(oldPriceList);

        final List<PriceDto> newPriceList = Stream.of(
                new PriceDto("122856", 1, 1, priceFormat.parse("20.01.2013 00:00:00"),
                        priceFormat.parse("20.02.2013 23:59:59"), 11000L),
                new PriceDto("122856", 2, 1, priceFormat.parse("15.01.2013 00:00:00"),
                        priceFormat.parse("25.01.2013 23:59:59"), 92000L),
                new PriceDto("6654", 1, 2, priceFormat.parse("12.01.2013 00:00:00"),
                        priceFormat.parse("13.01.2013 00:00:00"), 4000L)
        ).collect(Collectors.toList());

        final Set<Price> expected = Stream.of(
                new Price(1L, "122856", 1, 1, priceFormat.parse("01.01.2013 00:00:00"),
                        priceFormat.parse("20.02.2013 23:59:59"), 11000L),
                new Price(2L, "122856", 2, 1, priceFormat.parse("10.01.2013 00:00:00"),
                        priceFormat.parse("14.01.2013 23:59:59"), 99000L),
                new Price("122856", 2, 1, priceFormat.parse("15.01.2013 00:00:00"),
                        priceFormat.parse("25.01.2013 23:59:59"), 92000L),
                new Price("6654", 1, 2, priceFormat.parse("01.01.2013 00:00:00"),
                        priceFormat.parse("11.01.2013 23:59:59"), 5000L),
                new Price("6654", 1, 2, priceFormat.parse("12.01.2013 00:00:00"),
                        priceFormat.parse("13.01.2013 00:00:00"), 4000L),
                new Price(3L, "6654", 1, 2, priceFormat.parse("13.01.2013 00:00:01"),
                        priceFormat.parse("31.01.2013 00:00:00"), 5000L)
        ).collect(Collectors.toSet());

        priceService.createPriceBatch(newPriceList);

        verify(priceRepositoryMock, times(1)).save(eq(expected));
        verify(priceRepositoryMock, times(0)).deleteAllByIdIn(eq(Collections.emptyList()));
    }

}
